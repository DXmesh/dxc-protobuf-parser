

#[derive(Debug, thiserror::Error)]
#[error("Cycle detected")]
pub struct TopoSortCycle;

// pub fn toposort<K, I>(
//     input: impl IntoIterator<Item = K>,
//     deps: impl Fn(&K) -> I,
// ) -> Result<Vec<K>, TopoSortCycle>
// where
//     K: Eq + Hash + Clone,
//     I: Iterator<Item = K>,
// {
//     struct Ts<K, D, I>
//     where
//         K: Eq + Hash + Clone,
//         I: Iterator<Item = K>,
//         D: Fn(&K) -> I,
//     {
//         result_set: HashSet<K>,
//         result: Vec<K>,
//         deps: D,
//         stack: HashSet<K>,
//     }

//     impl<K, D, I> Ts<K, D, I>
//     where
//         K: Eq + Hash + Clone,
//         I: Iterator<Item = K>,
//         D: Fn(&K) -> I,
//     {
//         fn visit(&mut self, i: &K) -> Result<(), TopoSortCycle> {
//             if self.result_set.contains(i) {
//                 return Ok(());
//             }

//             if !self.stack.insert(i.clone()) {
//                 return Err(TopoSortCycle);
//             }
//             for dep in (self.deps)(i) {
//                 self.visit(&dep)?;
//             }

//             let removed = self.stack.remove(i);
//             assert!(removed);

//             self.result.push(i.clone());
//             self.result_set.insert(i.clone());

//             Ok(())
//         }
//     }

//     let mut ts = Ts {
//         result: Vec::new(),
//         result_set: HashSet::new(),
//         deps,
//         stack: HashSet::new(),
//     };

//     for i in input {
//         ts.visit(&i)?;
//     }

//     Ok(ts.result)
// }
